import * as types from "./../constants/ActionType";
const initialState = [];
const product = (state = initialState, action) => {
  switch (action.type) {
    case types.DATA_API:
      //console.log(state);
      state = action.showDataListApi;
      return [...state];
    default:
      return [...state];
  }
};
export default product;
