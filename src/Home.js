import React, { Component } from "react";
import SideBar from "./components/SideBar";
import Header from "./components/Header";
import MainContent from "./components/MainContent";
export class Home extends Component {
  render() {
    return (
      <div id="wrapper">
        <SideBar />
        <div id="content-wrapper" className="d-flex flex-column">
          {/* Main Content */}
          <div id="content">
            <Header />
            <div className="container-fluid px-2">
              <MainContent />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
