import React from "react";
import "./App.css";
import { BrowserRouter as Router, NavLink } from "react-router-dom";
import RouterURL from "./components/RouterURL";
function App() {
  return (
    <Router>
      <RouterURL />
    </Router>
  );
}

export default App;
