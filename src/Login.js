import React, { Component } from "react";

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txtUser: "",
      txtPass: "",
      status: false
    };
  }
  onChange = () => {
    this.setState({
      txtUser: this.refs.txtUser.value,
      txtPass: this.refs.txtPass.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const arr = [];
    const data = {
      name: "admin",
      pass: "admin"
    };
    arr.push(data);
    const dataInput = {
      txtUser: this.state.txtUser,
      txtPass: this.state.txtPass
    };
    console.log(dataInput.txtUser);
    var s = false;
    if (
      dataInput.txtUser === null ||
      dataInput.txtUser === "" ||
      dataInput.txtUser === undefined
    ) {
      document.getElementById("result").innerHTML =
        "Username không được để trống";
      return;
    }
    if (
      dataInput.txtPass === null ||
      dataInput.txtPass === "" ||
      dataInput.txtPass === undefined
    ) {
      document.getElementById("result").innerHTML =
        "Password không được để trống";
      return;
    }

    for (let i = 0; i < arr.length; i++) {
      if (
        arr[i].name === dataInput.txtUser &&
        arr[i].pass === dataInput.txtPass
      ) {
        s = true;
      }
    }

    if (s == true) {
      document.getElementById("result").innerHTML = "Đăng nhập thành công";
      return (window.location.href = "/product");
    } else {
      document.getElementById("result").innerHTML = "Đăng nhập thất bại";
    }
  };
  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-4">
            <h1 className="h1 my-4 text-center">LOGIN</h1>
            <form>
              <div className="form-group">
                {this.state.txtUser},{this.state.txtPass} <br />
                <label for="exampleInputEmail1">UserName</label>
                <input
                  type="text"
                  className="form-control"
                  id="username"
                  ref="txtUser"
                  onChange={this.onChange}
                  placeholder="UserName"
                />
              </div>
              <div className="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  ref="txtPass"
                  onChange={this.onChange}
                  placeholder="Password"
                />
              </div>
              <div className="text-center">
                <button
                  onClick={this.onSubmit}
                  type="submit"
                  className="btn btn-primary "
                >
                  Submit
                </button>
              </div>
              <p id="result" className="text-danger"></p>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
