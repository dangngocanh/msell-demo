import * as types from "./../constants/ActionType";
const actAddToItem = showDataListApi => {
  return {
    type: types.DATA_API,
    showDataListApi
  };
};
export default actAddToItem;
