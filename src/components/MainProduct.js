import React, { Component } from "react";
import SideBar from "./SideBar";
import Header from "./Header";
import Product from "./Product";

class MainProduct extends Component {
  render() {
    return (
      <div id="wrapper">
        <SideBar />
        <div id="content-wrapper" className="d-flex flex-column">
          {/* Main Content */}
          <div id="content">
            <Header />
            <div className="container-fluid px-2">
              <Product />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MainProduct;
