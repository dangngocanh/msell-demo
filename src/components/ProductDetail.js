import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

export class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: []
    };
  }

  componentDidMount() {
    var idProducts = this.props.match.params.id;
    axios
      .get(`http://5da98602de10b40014f371cc.mockapi.io/msell/${idProducts}`)
      .then(res => {
        const persons = res.data;

        this.setState({ persons });
      })
      .catch(error => console.log(error));
  }
  censorship = () => {
    // persons.trang_thai = "Đã duyệt";
  };
  render() {
    const { match } = this.props;
    console.log(match);
    const { persons } = this.state;
    console.log(persons);
    // const showData = persons.map(e => {
    //   return <p>aaa</p>;
    // });

    return (
      <div className="card mb-4">
        <div className="card-body ">
          <div className="row">
            <div className="col-8">abc</div>
            <div className="col-4">
              <div className="card">
                <input type="text" placeholder="Tìm kiếm" />
              </div>
            </div>
          </div>
          <table className="table table-hover">
            <thead className="thead-light">
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Mã sản phẩm</th>
                <th scope="col">Ngày đăng</th>
                <th scope="col">Khu vực</th>
                <th scope="col">Trạng thái</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">{persons.id}</th>
                <td>{persons.ma_san_pham}</td>
                <td>{persons.ngay_bat_dau}</td>
                <td>{persons.khu_vuc}</td>
                <td>
                  <p
                    className={
                      persons.trang_thai !== "Đợi duyệt"
                        ? "text-success"
                        : "text-warning"
                    }
                  >
                    {persons.trang_thai}
                  </p>
                </td>
                <td>
                  <div className="item-product d-flex ">
                    <div
                      className="item-censorship mr-4 d-flex flex-column align-items-center"
                      onClick={() => this.censorship(persons.trang_thai)}
                    >
                      <i class="fa fa-check" aria-hidden="true"></i>
                      Duyệt
                    </div>
                    <div className="item-delete mr-4 d-flex flex-column align-items-center">
                      <i className="fa fa-trash-o" aria-hidden="true"></i>
                      Xóa
                    </div>
                    <div className="item-update d-flex flex-column align-items-center">
                      <i
                        className="fa fa-pencil-square-o"
                        aria-hidden="true"
                      ></i>
                      Sửa
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          <div className="row">
            <div className="col-12">
              <p>
                <strong>Tiêu đề</strong> : {persons.tieu_de}
              </p>
            </div>
            <div className="col">
              <div className="row">
                <div className="col-3">
                  <p>
                    <strong>Giá</strong> : {persons.tieu_de}
                  </p>
                </div>
                <div className="col-3">
                  <p>
                    <strong>Diện tích</strong> : {persons.dien_tich}
                  </p>
                </div>
                <div className="col-3">
                  <p>
                    <strong>Hình thức</strong> : {persons.hinh_thuc}
                  </p>
                </div>
                <div className="col-3">
                  <p>
                    <strong>Loại BĐS</strong> : {persons.loai_dat}
                  </p>
                </div>
              </div>
            </div>

            <div className="col-12">
              <p>
                <strong>Địa chỉ</strong> : {persons.tieu_de}
              </p>
            </div>
            <div className="col-12">
              <div className="row">
                <div className="col-4">
                  <p>
                    <strong>Gói tin đăng</strong> : {persons.tieu_de}
                  </p>
                </div>
                <div className="col-4">
                  <p>
                    <strong>Ngày bắt đầu</strong> : {persons.ngay_bat_dau}
                  </p>
                </div>
                <div className="col-4">
                  <p>
                    <strong>Ngày kết thúc</strong> : {persons.ngay_ket_thuc}
                  </p>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="row">
                <div className="col-6 ">
                  <table className="tableminilist">
                    <tbody>
                      <tr className="">
                        <td>Số tầng</td>
                        <td className="text-right">{persons.so_tang}</td>
                      </tr>
                      <tr>
                        <td>Số phòng ngủ</td>
                        <td className="text-right">{persons.so_phong_ngu}</td>
                      </tr>
                      <tr>
                        <td>Số phòng tắm</td>
                        <td className="text-right">{persons.so_phong_tam}</td>
                      </tr>
                      <tr>
                        <td>Số tolet</td>
                        <td className="text-right">{persons.so_tolet}</td>
                      </tr>
                      <tr>
                        <td>Hướng nhà</td>
                        <td className="text-right">{persons.huong_nha}</td>
                      </tr>
                      <tr>
                        <td>Hướng ban công</td>
                        <td className="text-right">{persons.huong_ban_cong}</td>
                      </tr>
                      <tr>
                        <td>Mặt tiền rộng</td>
                        <td className="text-right">{persons.mat_tien_rong}</td>
                      </tr>
                      <tr>
                        <td>Đường trước cửa</td>
                        <td className="text-right">
                          {persons.duong_truoc_cua}
                        </td>
                      </tr>
                      <tr>
                        <td>Pháp lý</td>
                        <td className="text-right"> {persons.phap_ly}</td>
                      </tr>
                      <tr>
                        <td>Nội thất</td>
                        <td className="text-right">{persons.noi_that}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="col-6"></div>
              </div>
            </div>
            <div className="col-12 d-flex justify-content-center mt-4">
              {persons.trang_thai !== "Đợi duyệt" ? (
                <button type="button" className="btn btn-success mr-2" disabled>
                  Duyệt tin
                </button>
              ) : (
                <button
                  onClick={() => this.censorship()}
                  type="button"
                  className="btn btn-success mr-2"
                >
                  Duyệt tin
                </button>
              )}
              <button type="button" className="btn btn-danger mr-2">
                Gỡ tin
              </button>
              <button type="button" className="btn btn-info">
                Sửa tin
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductDetail;
