import React, { Component } from "react";
import SideBar from "./SideBar";
import Header from "./Header";
import UpdataProduct from "./UpdataProduct";

class MainUpdateProduct extends Component {
  render() {
    console.log(this.props);
    return (
      <div id="wrapper">
        <SideBar />
        <div id="content-wrapper" className="d-flex flex-column">
          {/* Main Content */}
          <div id="content">
            <Header />
            <div className="container-fluid px-2">
              <UpdataProduct match={this.props.match} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MainUpdateProduct;
