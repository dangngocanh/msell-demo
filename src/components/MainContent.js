import React, { Component } from "react";

export class MainContent extends Component {
  render() {
    return (
      <div className="card">
        <div className="card-body">This is some text within a card body.</div>
      </div>
    );
  }
}

export default MainContent;
