import React, { Component } from "react";
import axios from "axios";
export class CreateData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      tieu_de: "",
      mo_ta: "",
      dien_tich: "",
      hinh_thuc: "",
      loai_dat: "",
      tinh_thanh_pho: "",
      quan_huyen: "",
      xa_phuong: "",
      duong_pho: "",
      so_nha: "",
      so_tang: "",
      so_phong_ngu: "",
      so_phong_tam: "",
      mat_tien_rong: "",
      duong_truoc_cua: "",
      huong_nha: "",
      huong_ban_cong: "",
      phap_ly: "",
      noi_that: "",
      hinh_anh: "",
      ten_lien_he: "",
      so_dien_thoai: "",
      email: "",
      loai_tin_rao: "",
      ngay_bat_dau: "",
      ngay_ket_thuc: "",
      so_tolet: "",
      ma_san_pham: "",
      khu_vuc: "",
      trang_thai: "Đợi duyệt",
      date: new Date()
    };
  }
  makeid = length => {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
    //  console.log(makeid(5));
  };

  onSubmit = e => {
    e.preventDefault();
    console.log(this.state);
    try {
      const employee = {
        tieu_de: this.state.tieu_de,
        mo_ta: this.state.mo_ta,
        dien_tich: this.state.dien_tich,
        hinh_thuc: this.state.hinh_thuc,
        loai_dat: this.state.loai_dat,
        tinh_thanh_pho: this.state.tinh_thanh_pho,
        quan_huyen: this.state.quan_huyen,
        xa_phuong: this.state.xa_phuong,
        duong_pho: this.state.duong_pho,
        so_nha: this.state.so_nha,
        so_tang: this.state.so_tang,
        so_phong_ngu: this.state.so_phong_ngu,
        so_phong_tam: this.state.so_phong_tam,
        mat_tien_rong: this.state.mat_tien_rong,
        duong_truoc_cua: this.state.duong_truoc_cua,
        huong_nha: this.state.huong_ban_cong,
        huong_ban_cong: this.state.huong_ban_cong,
        phap_ly: this.state.phap_ly,
        noi_that: this.state.noi_that,
        hinh_anh: this.state.hinh_anh,
        ten_lien_he: this.state.ten_lien_he,
        so_dien_thoai: this.state.so_dien_thoai,
        email: this.state.email,
        loai_tin_rao: this.state.loai_tin_rao,
        ngay_bat_dau: this.state.ngay_bat_dau,
        ngay_ket_thuc: this.state.ngay_ket_thuc,
        so_tolet: this.state.so_tolet,
        khu_vuc: this.state.khu_vuc,
        trang_thai: this.state.trang_thai,
        ma_san_pham: this.makeid(30),
        date: this.state.date
      };
      axios.post("http://5da98602de10b40014f371cc.mockapi.io/msell/", employee);
      alert("Tạo thành công").then(res => console.log(res.data));
    } catch (error) {
      console.log("lỗi");
    }
  };
  onChangeValue = event => {
    var value = event.target.value;
    var name = event.target.name;
    //console.log(value, name);
    this.setState({
      [name]: value
    });
    console.log(this.state);
  };
  render() {
    return (
      <div className="card">
        <div className="card-body">
          <h1 className="h5">THÔNG TIN CƠ BẢN</h1>
          <form autoCapitalize="off" className="needs-validation" noValidate>
            <div className="form-group ">
              <div className="mb-2 row">
                <label className="col-sm-2 col-form-label">Tiêu đề(*)</label>
                <div className="col-sm-10">
                  <input
                    type="text"
                    className="form-control"
                    name="tieu_de"
                    id=""
                    required
                    onChange={this.onChangeValue}
                    placeholder=""
                  />
                  <div className="valid-feedback">Looks good!</div>
                </div>
              </div>
              <div className="mb-2 row">
                <label className="col-sm-2 col-form-label">Mô tả(*)</label>
                <div className="col-sm-7">
                  <textarea
                    type="text"
                    className="form-control"
                    id=""
                    name="mo_ta"
                    required
                    placeholder=""
                    onChange={this.onChangeValue}
                  />
                </div>
                <div className="col-sm-3">
                  Giới thiệu chung bất động sản của bạn
                </div>
              </div>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">Diện tích</label>
                    <div className="col-sm-8">
                      <input
                        type="text"
                        className="form-control"
                        id=""
                        required
                        name="dien_tich"
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">Giá</label>
                    <div className="col-sm-8">
                      <input
                        name="gia"
                        required
                        type="text"
                        className="form-control"
                        id=""
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Hình thức (*)
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="hinh_thuc"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Loại đất(*)
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="loai_dat"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Tỉnh/Thành phố(*)
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="tinh_thanh_pho"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Quận/Huyện(*)
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="quan_huyen"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Xã/Phường(*)
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="xa_phuong"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Đường/Phó(*)
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="duong_pho"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-2 row">
                <label className="col-sm-2 col-form-label">Số nhà</label>
                <div className="col-sm-10">
                  <input
                    type="text"
                    name="so_nha"
                    className="form-control"
                    id=""
                    required
                    onChange={this.onChangeValue}
                    placeholder=""
                  />
                </div>
              </div>
              <h2 className="h5">Địa chỉ hiển thị</h2>
              <label>Vị trí</label>
              <h2 className="h5">THÔNG TIN CHI TIẾT</h2>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">Số tầng</label>
                    <div className="col-sm-8">
                      <input
                        name="so_tang"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Số phòng ngủ
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="so_phong_ngu"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Số phòng tắm
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="so_phong_tam"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Mặt tiền rộng
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="mat_tien_rong"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Đường trước cửa
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="duong_truoc_cua"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        placeholder=""
                        onChange={this.onChangeValue}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">Số tolet</label>
                    <div className="col-sm-8">
                      <input
                        name="so_tolet"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        placeholder=""
                        onChange={this.onChangeValue}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">Hướng nhà</label>
                    <div className="col-sm-8">
                      <input
                        name="huong_nha"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        placeholder=""
                        onChange={this.onChangeValue}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Hướng ban công
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="huong_ban_cong"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">Pháp lý</label>
                    <div className="col-sm-8">
                      <input
                        name="phap_ly"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">Nội thất</label>
                    <div className="col-sm-8">
                      <input
                        type="text"
                        name="noi_that"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
              </div>
              <h2 className="h5">HÌNH ẢNH</h2>
              <h2 className="h5">LIÊN HỆ</h2>
              <div className="mb-2 row">
                <label className="col-sm-2 col-form-label">
                  {" "}
                  Tên liên hệ(*)
                </label>
                <div className="col-sm-10">
                  <input
                    type="text"
                    name="ten_lien_he"
                    className="form-control"
                    id=""
                    required
                    onChange={this.onChangeValue}
                    placeholder=""
                  />
                </div>
              </div>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Số điện thoại
                    </label>
                    <div className="col-sm-8">
                      <input
                        name="so_dien_thoai"
                        type="text"
                        className="form-control"
                        id=""
                        required
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Địa chỉ Email
                    </label>
                    <div className="col-sm-8">
                      <input
                        type="text"
                        className="form-control"
                        id=""
                        required
                        name="email"
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
              </div>
              <h2 className="h5">LỊCH ĐĂNG TIN</h2>
              <div className="mb-2 row">
                <label className="col-sm-2 col-form-label">Loại tin rao</label>
                <div className="col-sm-6">
                  <input
                    type="text"
                    className="form-control"
                    id=""
                    required
                    name="loai_tin_rao"
                    onChange={this.onChangeValue}
                    placeholder=""
                  />
                </div>
              </div>
              <div className="mb-2 row">
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Ngày bắt đầu
                    </label>
                    <div className="col-sm-8">
                      <input
                        type="date"
                        required
                        className="form-control"
                        id=""
                        name="ngay_bat_dau"
                        onChange={this.onChangeValue}
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="row">
                    <label className="col-sm-4 col-form-label">
                      Ngày kết thúc
                    </label>
                    <div className="col-sm-8">
                      <input
                        type="date"
                        className="form-control"
                        id=""
                        required
                        name="ngay_ket_thuc"
                        placeholder=""
                        onChange={this.onChangeValue}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-12 d-flex justify-content-center mt-4">
                  <button
                    type="submit"
                    className="btn btn-success"
                    onClick={this.onSubmit}
                  >
                    Đăng tin
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default CreateData;
