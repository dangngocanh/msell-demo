import React, { Component } from "react";

export class PacketPosts extends Component {
  render() {
    return (
      <div className="card">
        <div className="card-body">
          <div className="row">
            <div className="col-8">
              <div className="d-flex">
                <select className="rounded-pill mr-2 p-1 mb-3">
                  <option value="boloc">Bộ Lọc</option>
                  <option value="Hà Nội">Hà Nội</option>
                  <option value="Hồ Chí Minh">Hồ Chí Minh</option>
                </select>
                <div>
                  {" "}
                  <div></div>
                </div>
              </div>
            </div>
            <div class="col-4">
              <div className="">
                <input
                  className="rounded-pill search-product w-100"
                  type="text"
                  placeholder="Tìm kiếm"
                />
              </div>
            </div>
          </div>
          <table className="table table-hover">
            <thead className="thead-light">
              <tr>
                <th scope="col">STT</th>
                <th scope="col">Mã gói</th>
                <th scope="col">Tên gói</th>
                <th scope="col">Giá gói/ngày</th>
                <th scope="col">Giới hạn</th>
                <th scope="col">Ngày tạo</th>
                <th scope="col">Trạng thái</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">3</th>
                <td>
                  <a href="/">Mã gói 3</a>
                </td>
                <td>Tên gói </td>
                <td>Giá gói</td>
                <td>7 ngày</td>
                <td>28/10/2019</td>
                <td>Đang hoạt động</td>
                <td>
                  <div class="item-product d-flex ">
                    <div className="item-delete mr-4 d-flex flex-column align-items-center">
                      <i className="fa fa-trash-o" aria-hidden="true"></i>Xóa
                    </div>
                    <div className="item-update d-flex flex-column align-items-center">
                      <a href="/product-update/masanpham-3.3">
                        <i
                          className="fa fa-pencil-square-o"
                          aria-hidden="true"
                        ></i>
                      </a>
                      Sửa
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default PacketPosts;
