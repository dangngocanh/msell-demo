import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";

export class UpdataProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: [],
      khu_vuc: "",
      show: false
    };
  }

  componentDidMount() {
    var idProducts = this.props.match.params.id;
    axios
      .get(`http://5da98602de10b40014f371cc.mockapi.io/msell/${idProducts}`)
      .then(res => {
        const persons = res.data;

        this.setState({ persons, khu_vuc: persons.khu_vuc });
      })
      .catch(error => console.log(error));
  }
  onChangeValue = event => {
    var value = event.target.value;
    var name = event.target.name;
    //console.log(value, name);
    this.setState({
      [name]: value
    });
    console.log(this.state);
  };

  // ---------------------------------
  onSubmit(e) {
    this.setState({
      show: true
    });
    // alert("Update thành công");
    try {
      //const { persons } = this.state;
      var idProducts = this.props.match.params.id;
      e.preventDefault();
      const productUpadate = {
        khu_vuc: this.state.khu_vuc
      };
      console.log(this.state);
      axios
        .put(
          `http://5da98602de10b40014f371cc.mockapi.io/msell/${idProducts}`,
          productUpadate
        )
        .then(res => {
          this.setState({
            show: false
          });

          console.log(res.data);
        });
    } catch (error) {
      console.log("Lỗi");
    }
  }

  render() {
    console.log(this.props);
    const { show } = this.state;
    return (
      <div className="card">
        <div className="card-body">
          <div>Thông tin cơ bản </div>
          <from>
            <div className="form-group row">
              <label className="col-sm-2 col-form-label">khuvuc:</label>
              <div className="col-sm-10">
                <input
                  type="text"
                  className="form-control"
                  id=""
                  name="khu_vuc"
                  defaultValue={this.state.khu_vuc}
                  onChange={this.onChangeValue}
                  placeholder=""
                />
              </div>
            </div>
            <button
              type="submit"
              onClick={event => this.onSubmit(event)}
              class="btn btn-success"
            >
              {show === true ? <i class="fa fa-refresh fa-spin"></i> : ""}
              Update
            </button>
          </from>
        </div>
      </div>
    );
  }
}

// const mapDispatchToProps = (dispatch, ownProps) => {
//     return {
//       showListData: dataApi => {
//         dispatch(actAddToItem(dataApi));
//       }
//     };
//   };
// mapStateToProps chuyển các state sang props lấy ra

const mapStateToProps = (data, ownProps) => {
  console.log(data.product);
  return {
    stateProps: data.product
  };
};
export default connect(
  mapStateToProps,
  null
)(UpdataProduct);
