import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "../Home";
import MainProduct from "./MainProduct";
import MainUpdateProduct from "./MainUpdateProduct";
import MainProductDetail from "./MainProductDetail";
import MainCreateData from "./MainCreateData";
import MainPost from "./MainPost";
import Login from "../Login";

export class RouterURL extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/" component={Login} />
          <Route exact path="/product" component={MainProduct} />
          <Route exact path="/posts" component={MainPost} />
          <Route
            exact
            path="/product-update/:slug.:id"
            component={MainUpdateProduct}
          />
          <Route
            exact
            path="/product-detail/:slug.:id"
            component={MainProductDetail}
          />
          <Route exact path="/create-product" component={MainCreateData} />
        </Switch>
      </Router>
    );
  }
}

export default RouterURL;
