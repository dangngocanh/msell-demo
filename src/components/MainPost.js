import React, { Component } from "react";
import SideBar from "./SideBar";
import Header from "./Header";
import PacketPosts from "./PacketPosts";

export class MainPost extends Component {
  render() {
    return (
      <div id="wrapper">
        <SideBar />
        <div id="content-wrapper" className="d-flex flex-column">
          {/* Main Content */}
          <div id="content">
            <Header />
            <div className="container-fluid px-2">
              <PacketPosts />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MainPost;
