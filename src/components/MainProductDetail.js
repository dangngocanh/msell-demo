import React, { Component } from "react";
import SideBar from "./SideBar";
import Header from "./Header";
import ProductDetail from "./ProductDetail";
export class MainProductDetail extends Component {
  render() {
    return (
      <div id="wrapper">
        <SideBar />
        <div id="content-wrapper" className="d-flex flex-column">
          {/* Main Content */}
          <div id="content">
            <Header />
            <div className="container-fluid">
              <ProductDetail match={this.props.match} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MainProductDetail;
