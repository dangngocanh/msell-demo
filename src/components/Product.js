import React, { Component } from "react";
import { connect } from "react-redux";
import actAddToItem from "./../actions/myAction";
import callApi from "./DataAPI";
import Pagination from "react-js-pagination";
import { Link } from "react-router-dom";
import axios from "axios";
export class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: [],
      value: "",
      data: [],
      arr: [],
      dataFilter: [],
      status: true,
      activePage: 1,
      totalItemsCount: 0,
      limit: 5,
      totalPage: 0,
      totalLength: ""
    };
  }

  componentDidMount() {
    callApi.all().then(persons => {
      // console.log(persons);
      this.setState({
        persons,
        totalItemsCount: persons.length,
        status: false,
        dataFilter: persons
      });
      console.log("didmout", persons.trang_thai);

      this.props.showListData(persons);
    });
  }
  // componentWillReceiveProps(nextProps) {
  //   console.log(nextProps.stateProps);
  //   this.setState({
  //     data: nextProps.stateProps
  //   });
  // }
  slugURL = str => {
    // Chuyển hết sang chữ thường
    str = str.toLowerCase();

    // xóa dấu
    str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, "a");
    str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, "e");
    str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, "i");
    str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, "o");
    str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, "u");
    str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, "y");
    str = str.replace(/(đ)/g, "d");

    // Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, "");

    // Xóa khoảng trắng thay bằng ký tự -
    str = str.replace(/(\s+)/g, "-");

    // xóa phần dự - ở đầu
    str = str.replace(/^-+/g, "");

    // xóa phần dư - ở cuối
    str = str.replace(/-+$/g, "");

    // return
    return str;
  };

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    var page = pageNumber;
    var perPage = this.state.limit; // hiển thị bn item
    var begin = (page - 1) * perPage;
    var end = page * perPage;
    const arr = this.state.persons.slice(begin, end);

    this.setState({
      totalLength: arr.length,
      dataFilter: arr,
      activePage: pageNumber
    });
    //this.setState({activePage: pageNumber});
    //  console.log(abc)
  }
  upDataData = () => {};
  isChange = e => {
    var value = e.target.value;
    this.setState(
      {
        value: value
      },
      () => {
        var { arr, value, persons } = this.state;
        if (value !== "boloc") {
          if (arr.indexOf(value) < 0) {
            arr.push(value);
            this.setState({
              value: arr
            });
            console.log(arr);
            const dataFinal = persons.filter(e => arr.indexOf(e.khu_vuc) >= 0);
            console.log("data:", dataFinal);
            this.setState(
              {
                dataFilter: dataFinal,
                totalItemsCount: dataFinal.length
              },
              () => {
                console.log(this.state.dataFilter);
              }
            );
          }
        }
      }
    );
  };
  delete = data => {
    const { arr, dataFilter, persons } = this.state;
    const index = arr.indexOf(data); // -1 or >= 0
    if (index >= 0) {
      arr.splice(index, 1);
      // let dataFilterFinal = []
      // dataFilter.forEach(e => {
      //   if(data !== e.quequan){
      //     dataFilterFinal.push(e)
      //   }
      // })
      // de loc ra nhung phan tu co trong mag khong co ky tu vua dc chon de xoa'
      const dataFilterFinal = dataFilter.filter(
        e => data.indexOf(e.khu_vuc) < 0
      );
      console.log(dataFilterFinal, data);
      this.setState({
        arr,
        dataFilter: dataFilterFinal
      });
      if (arr.length <= 0) {
        this.setState({
          dataFilter: persons
        });
      }
    }
  };
  onChangeData = e => {
    var { value } = e.target;
    var { persons } = this.state;
    let arr = [];
    persons.forEach(e => {
      if (e.ma_san_pham.toLowerCase().indexOf(value.toLowerCase()) > -1) {
        arr.push(e);
      }
    });
    const arr2 = arr.slice(0, 5);
    this.setState({
      dataFilter: arr2
    });
  };
  deleteData = (e, i) => {
    var { dataFilter } = this.state;
    dataFilter.splice(i, 1);

    if (window.confirm("Bạn có muốn xóa không????") === true) {
      axios
        .delete(`http://5da98602de10b40014f371cc.mockapi.io/msell/${e}`)
        .then(res => {
          this.setState({
            dataFilter,
            totalItemsCount: dataFilter.length
          });
        });
    } else {
      return false;
    }
  };

  censorship = (e, i) => {
    var { dataFilter } = this.state;
    //const { persons } = this.state;
    var idProducts = e;

    const productUpadate = {
      trang_thai: "Đã duyệt"
    };
    console.log(this.state);
    axios
      .put(
        `http://5da98602de10b40014f371cc.mockapi.io/msell/${idProducts}`,
        productUpadate
      )
      .then(res => {
        dataFilter[i].trang_thai = res.data.trang_thai;
        this.setState({
          dataFilter
        });
        console.log(dataFilter);
      });
  };

  render() {
    const { status, dataFilter, arr, totalLength } = this.state;
    console.log(dataFilter);
    dataFilter.map(e => (e.date = Date.parse(e.date)));
    const api = dataFilter.sort((a, b) => b.date - a.date);
    const dataApi = api.slice(0, 5);
    const showListData = dataApi.map((e, i) => {
      return (
        <tr key={i}>
          <th className="text-center" scope="row">
            {e.id}
          </th>
          <td>
            <Link
              to={"/product-detail/" + this.slugURL(e.ma_san_pham) + "." + e.id}
            >
              {e.ma_san_pham}
            </Link>
          </td>
          <td>{e.ngay_bat_dau}</td>
          <td>{e.khu_vuc}</td>
          <td>
            <p
              className={
                e.trang_thai !== "Đợi duyệt"
                  ? "font-italic text-success "
                  : "font-italic text-warning"
              }
            >
              {e.trang_thai}
            </p>
          </td>
          <td>
            <div className="item-product d-flex ">
              <div
                className={` ${
                  e.trang_thai === "Đợi duyệt"
                    ? "item-censorship"
                    : "item-censorship-display"
                } mr-4 d-flex flex-column align-items-center`}
                onClick={() => this.censorship(e.id, i)}
              >
                <i class="fa fa-check" aria-hidden="true"></i>
                Duyệt
              </div>
              <div className="item-delete mr-4 d-flex flex-column align-items-center">
                <i
                  className="fa fa-trash-o"
                  aria-hidden="true"
                  onClick={() => this.deleteData(e.id, i)}
                ></i>
                Xóa
              </div>
              <div className="item-update d-flex flex-column align-items-center">
                <Link
                  to={
                    "/product-update/" +
                    this.slugURL(e.ma_san_pham) +
                    "." +
                    e.id
                  }
                >
                  <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                </Link>
                Sửa
              </div>
            </div>
          </td>
        </tr>
      );
    });
    return (
      <div className="card py-3 mb-5">
        <div className="card-body p-0">
          <div className="row m-0">
            <div className="col-8">
              <div className="d-flex">
                <select
                  onChange={this.isChange.bind(this)}
                  className="rounded-pill mr-2 p-1 mb-3"
                >
                  <option value="boloc">Bộ Lọc</option>
                  <option value="Hà Nội">Hà Nội</option>
                  <option value="Hồ Chí Minh">Hồ Chí Minh</option>
                </select>
                <div>
                  {" "}
                  <div>
                    {arr.map((e, i) => {
                      return (
                        <button
                          key={i}
                          onClick={() => this.delete(e)}
                          type="button"
                          class="rounded-pill btn btn-primary mr-2 "
                        >
                          <i
                            className="fa fa-times-circle-o pr-2"
                            aria-hidden="true"
                          ></i>
                          {e}
                        </button>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-4">
              <div className="search-input">
                <i class="fa fa-search" aria-hidden="true"></i>
                <input
                  className="rounded-pill search-product w-100"
                  onChange={this.onChangeData}
                  type="text"
                  placeholder="Tìm kiếm"
                />
              </div>
            </div>
          </div>

          <table className="table table-striped-even table-borderless">
            <thead className="thead-light">
              <tr>
                <th className="text-center" scope="col ">
                  STT
                </th>
                <th scope="col">Mã sản phẩm</th>
                <th scope="col">Ngày đăng</th>
                <th scope="col">Khu vực</th>
                <th scope="col">Trạng thái</th>
                <th></th>
              </tr>
            </thead>
            <tbody>{status === true ? "Loandding" : showListData}</tbody>
          </table>
          <div className="row justify-content-end m-0">
            <div className="col-4">
              <div className="float-right">
                <Pagination
                  //hideDisabled
                  hideFirstLastPages
                  prevPageText="prev"
                  nextPageText="next"
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.limit}
                  totalItemsCount={this.state.totalItemsCount}
                  pageRangeDisplayed={this.state.totalPage}
                  innerClass={"pagination justify-content-end"}
                  linkClass={"page-link"}
                  activeClass={"active"}
                  itemClass={"paginate_button page-item "}
                  onChange={e => this.handlePageChange(e)}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    showListData: dataApi => {
      dispatch(actAddToItem(dataApi));
    }
  };
};

const mapStateToProps = (data, ownProps) => {
  //  console.log(data.product);
  return {
    stateProps: data.product
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Product);
