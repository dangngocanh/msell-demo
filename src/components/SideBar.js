import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";
class SideBar extends Component {
  render() {
    return (
      <ul
        className="navbar-nav sidebar sidebar-dark accordion"
        id="accordionSidebar"
      >
        <NavLink
          className="sidebar-brand d-flex align-items-center justify-content-center"
          to="/"
        >
          <div className="sidebar-brand-text mx-3">MSELL</div>
        </NavLink>
        <li className="nav-item">
          <NavLink activeClassName="active" className="nav-link" to="/product">
            <i className="fas fa-fw fa-tachometer-alt" />
            <span>Tin đăng</span>
          </NavLink>
        </li>
        {/* Nav Item - Tables */}
        <li className="nav-item">
          <NavLink activeClassName="active" className="nav-link" to="/posts">
            <i className="fas fa-fw fa-table" />
            <span>Gói tin đăng</span>
          </NavLink>
        </li>
        {/* Divider */}
      </ul>
    );
  }
}

export default SideBar;
